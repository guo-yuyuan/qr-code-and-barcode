import barcode
from barcode.writer import ImageWriter

print(f"python-barcode支持的条形码格式：\n{barcode.PROVIDED_BARCODES}") #查看 python-barcode 支持的条形码格式

# 更多了解：https://pypi.org/project/python-barcode/0.8.1/
"""
barcode.get（name,code,writer_mode,set_style,bottom_text）
参数解释：
name：条形码格式；-->必选参数
ode：条形码内容;--->必选参数
[ImageWriter或SVGWriter：要使用的写入程序；默认是SVGWriter
set_style：（字典）生成条形码图片的设置样式； 默认是default_writer_options这个变量
bottom_text：条形码底部显示的文本]
"""
# 创建条形码格式对象，参数为支持的格式
EAN = barcode.get_barcode_class('code39')

# 条形码内容 693 2021.11.5 23:38 2
"""
自定义条形码内容：
693:中国的国家代码之一
2021.11.5 写代码的时间
23:38:写代码的时间
2：由前面12位数字依据一定的算法计算得到 6+9+3-2-0-2-1-1-1-5-2+3+3-8
"""
message = "693202111523382"

# 创建条形码对象
"""
EAN():
参数write 为 NONE,保存图像文件格式则是 svg,
参数为ImageWrite()则默认图片格式是 PNG,需要改成JPEG等,则需要在options={“format”: “JPEG”},进行设置
"""
ean = EAN(message,writer=ImageWriter())

# 保存条形码图片，并且返回路径
'''
save函数有两个参数：save(filename,options=None)
filename参数为保存文件名，不需要加扩展名，将根据设置自动添加扩展名，由函数返回文件全名。当前面构造函数使用默认writer时，保存为SVG文件，扩展名为.svg。
options参数默认值为None，此时使用默认参数。如果需要修改设置，使用字典传入参数，例如：save("d:\\barcode",{'text': 'ABCD','format':'JPEG'})。可用参数如下：
    'module_width'：默认值0.2，每个条码宽度（？），单位为毫米
    'module_height'：默认值15.0，条码高度，单位为毫米
    'quiet_zone'：默认值6.5，两端空白宽度，单位为毫米
    'font_size'：默认值10，文本字体大小，单位为磅
    'text_distance'：默认值5.0，文本和条码之间的距离，单位为毫米
    'background'：默认值'white'，背景色
    'foreground'：默认值'black'，前景色
    'text'：默认值''，显示文本，默认显示编码，也可以自行设定
    'write_text'：默认值True，是否显示文本，如果为True自动生成text的值，如果为False则不生成（如果此时手工设置了text的值，仍然会显示文本）。
    'center_text'：默认值True，是否居中显示文本
    'format'：默认值'PNG'，保存文件格式，默认为PNG，也可以设为JPEG、BMP等，只在使用ImageWriter时有效。
    'dpi'：默认值300，图片分辨率，，只在使用ImageWriter时有效。
'''
fullname = ean.save("images/条形码")
print("条形码生成成功")
print(f"条形码路径：{fullname}")
print("="*10)

# =======================================================================

# 解析条形码
# 介绍： https://github.com/zxing/zxing/wiki/Getting-Started-Developing
import zxing

reader = zxing.BarCodeReader()
# 在decode函数中，用subprocess包中的Popen实现了调用jar包的操作，类似于VB,VC中的管道通讯。
barcode = reader.decode("images/条形码.png")
result = barcode.parsed
print(f"解析条形码的内容:{result}")