
# qrcode 普通二维码生成
# https://github.com/x-hw/amazing-qr
import qrcode
data = "Ordinary QR code generated successfully"  #二维码跳转链接或内容 不过好像中文不行
# 生成二维码
image = qrcode.make(data=data)
# 显示二维码
# image.show()
# 保存二维码
image.save("images/普通二维码生成.jpg")
print("普通二维码生成成功！！！")
print("="*10)

# =====================================================================

# qrcode 带颜色的二维码生成
import qrcode
"""
.QRCode()方法 参数：
version:二维码的格子矩阵大小，可以是1到40,1最小为21*21,40是177*177
error_correction：二维码错误容许率，默认 ERROR_CORRECT_M，容许小于 15% 的错误率
box_size：二维码每个小格子包含的像素数量
border：二维码到图片边框的小格子数，默认值为 4
"""
qr = qrcode.QRCode(version=1,
              error_correction=1,
              box_size=10,
              border=4
              )

data = "QR code with color generated successfully"  #二维码跳转连接或内容 不过好像中文不行
qr.add_data(data=data)

# 启用二维码颜色设置
qr.make(fit=True)
img = qr.make_image(fill_color="blue",back_color="white")

# 二维码显示
# img.show()

# 二维码图片保存
img.save("images/带颜色的二维码.jpg")
print("带颜色的二维码生成成功！！")
print("="*10)

# ===========================================================

# 带图的二维码
from MyQR import myqr
"""
words：二维码内容，链接或者句子
version：二维码大小，范围为[1,40]
level：二维码纠错级别，范围为{L,M,Q,H}，H为最高级，默认。
picture：自定义二维码背景图，支持格式为 .jpg，.png，.bmp，.gif，默认为黑白色
colorized：二维码背景颜色，默认为 False，即黑白色
contrast：对比度，值越高对比度越高，默认为 1.0
brightness：亮度，值越高亮度越高，默认为 1.0，值常和对比度相同
save_name：二维码名称，默认为 qrcode.png
save_dir：二维码路径，默认为程序工作路径
"""
# 二维码内容
words = "QR code with graph generated successfully"
myqr.run(
    words=words,
    version=1,
    picture='images/A1.png',
    colorized=True,
    save_name="images/带图片的二维码.png"
)
print("带图的二维码生成成功！！！")
print("="*10)

# =====================================================================

# 带动图的二维码

from MyQR import myqr
# 更多:https://pypi.org/project/MyQR/         https://github.com/sylnsfar/qrcode
words = "The QR code. GIF of the drawing was saved successfully"
myqr.run(
    words=words,
    version=1,
    picture="images/企鹅.gif",
    colorized=True,
    save_name="images/带动图的二维码.gif"

)
print("带动图的二维码.gif保存成功!!")
print("="*10)

# =======================================================================

# 解析二维码
# 介绍： https://github.com/zxing/zxing/wiki/Getting-Started-Developing
import zxing

reader = zxing.BarCodeReader()
# 在decode函数中，用subprocess包中的Popen实现了调用jar包的操作，类似于VB,VC中的管道通讯。
barcode = reader.decode("images/带动图的二维码.gif")
result = barcode.parsed
print(f"解析二维码内容:{result}")